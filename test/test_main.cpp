
//#define _TEST

#ifndef _TEST

#include <gtest.h>

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

#else

#include "tdatstack.h"
#include "tsimplestack.h"
#include <iostream>

int main()
{
	return 0;
}

#endif
//���� ������������� �����

#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#include <exception>
#include "tdatacom.h"

#define DefMemSize   25  // ������ ����� �� ���������

#define DataEmpty  -101  // ���� ������
#define DataFull   -102  // ���� �����������

template <class TypeData=int, int N=DefMemSize>
class TStack2 : public TDataCom
{
private:
	int DataCount; // ���������� ��������� � �����
	int Top; //������ ���������� ��������
	TypeData Data[N];
public:
	TStack2();
	void Put(const TypeData &);//�������� � ����� �������
	TypeData Get(void);//������� ��������� �������
	void Print();//������ �����
	bool IsEmpty()
	{
		return DataCount == 0;
	}
	TypeData HGet(void)
	{
		return Data[Top];
	}
};

template <class TypeData, int N>
TStack2 <TypeData, N> :: TStack2() : Top(-1), DataCount(0)
{
}

template <class TypeData, int N>
void TStack2 <TypeData, N> ::Put(const TypeData &Val)
{
	if (DataCount == N) SetRetCode(DataFull);
	else
	{
		Data[++Top] = Val;
		DataCount++;
	}
}

template <class TypeData, int N>
TypeData TStack2 <TypeData, N> ::Get(void)
{
	TypeData result = -1;
	if (DataCount == 0) SetRetCode(DataEmpty);
	else
	{
		result = Data[Top--];
		DataCount--;
	}
	return result;
}

template <class TypeData, int N>
void TStack2 <TypeData, N> :: Print()
{
	for (int i = 0; i < DataCount; i++)
		cout << Data[i] << ' ';
	cout << endl;
}

#endif